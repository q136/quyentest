package auto.assignment.scenariosteps.common;

import auto.assignment.ui.steps.VerificationBaseSteps;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

public class VerificationSteps {
	@Steps
	VerificationBaseSteps verificationSteps;


	@Then("^the text of \"(.*?)\" element should be \"(.*?)\"$")
	public void the_text_element_should_be(String target, String value) {
		verificationSteps.the_text_element_should_be(target, value);
	}

	@Then("^the text of \"(.*?)\" element should not be \"(.*?)\"$")
	public void the_text_element_should_not_be(String target, String value) {
		verificationSteps.the_text_element_should_not_be(target, value);
	}

	@Then("^the text of \"(.*?)\" element should contain \"(.*?)\"$")
	public void the_text_element_should_contain(String target, String value) {
		verificationSteps.the_text_element_should_contain(target, value);
	}

	@Then("^the text of \"(.*?)\" element should not contain \"(.*?)\"$")
	public void the_text_element_should_not_contain(String target, String value) {
		verificationSteps.the_text_element_should_not_contain(target, value);
	}
}
