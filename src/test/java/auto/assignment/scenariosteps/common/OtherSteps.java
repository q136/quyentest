package auto.assignment.scenariosteps.common;

import auto.assignment.ui.steps.EndUserBaseSteps;
import auto.assignment.utilities.TestLogger;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import net.thucydides.core.annotations.Steps;

public class OtherSteps {
	@Steps
	EndUserBaseSteps endUser;

	Scenario scenario;

	@Before
	public void beforeTest(Scenario scenario) {
		this.scenario = scenario;
		TestLogger.info("==========================================================================");
		TestLogger.info("     START TESTING SCENARIO " + scenario.getName() + "");
		TestLogger.info("==========================================================================");
	}

	@After
	public void tearDown(Scenario scenario) {
		endUser.close_browser();
		TestLogger.info("==========================================================================");
		TestLogger.info("                              FINISH TESTING                              ");
		TestLogger.info("==========================================================================");
	}


	@Given("clear session")
	public void clear_session() {
		endUser.clear_session();
	}

}
