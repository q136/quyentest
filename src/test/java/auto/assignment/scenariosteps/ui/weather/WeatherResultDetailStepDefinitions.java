package auto.assignment.scenariosteps.ui.weather;

import auto.assignment.ui.steps.VerificationBaseSteps;
import auto.assignment.utilities.Helper;
import io.cucumber.java.en.Then;
import net.thucydides.core.annotations.Steps;

import java.util.Date;

public class WeatherResultDetailStepDefinitions {
    @Steps
    VerificationBaseSteps verificationBaseSteps;
    private String TEMP_UNIT = "°C";

    // Locator keys
    private String SEARCH_RESULT_DETAIL_CURRENT_DATE_LABEL = "result.detail.currentdate.label";
    private String SEARCH_RESULT_DETAIL_CITY_LABEL = "result.detail.city.label";
    private String SEARCH_RESULT_DETAIL_TEMP_LABEL = "result.detail.temp.label";

    @Then("^Search detail should display current weather for location ['\"]([^'\"]*)['\"]$")
    public void verify_search_result(String location) {
        long now = new Date().getTime();
        verificationBaseSteps.the_text_element_should_be(SEARCH_RESULT_DETAIL_CURRENT_DATE_LABEL, Helper.formatDateTimeInMonthDateTime(now));
        verificationBaseSteps.the_text_element_should_be(SEARCH_RESULT_DETAIL_CITY_LABEL,location);
        verificationBaseSteps.the_text_element_should_contain(SEARCH_RESULT_DETAIL_TEMP_LABEL,TEMP_UNIT);
    }

}
