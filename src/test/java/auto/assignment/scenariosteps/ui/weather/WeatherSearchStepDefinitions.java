package auto.assignment.scenariosteps.ui.weather;

import auto.assignment.ui.steps.CoreActionFormBaseSteps;
import auto.assignment.ui.steps.VerificationBaseSteps;
import auto.assignment.ui.steps.WindowAndFrameBaseSteps;
import auto.assignment.utilities.Constant;
import auto.assignment.utilities.TestLogger;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import org.apache.commons.lang3.RandomStringUtils;

public class WeatherSearchStepDefinitions {
    @Steps
    CoreActionFormBaseSteps coreActionFormBaseSteps;

    @Steps
    VerificationBaseSteps verificationBaseSteps;

    @Steps
    WindowAndFrameBaseSteps windowAndFrameBaseSteps;

    // Locator keys
    private String SITE_URL = "application.url";
    private String SEARCH_TOP_TEXTBOX = "search.top.textbox";
    private String SEARCH_RESULT_LINK = "search.result.city.link";
    private String SEARCH_RESULT_NOTFOUND_LABEL = "search.result.notfound.label";

    private String SEARCH_RESULT_NOTFOUND_DISPLAY_TEXT = "Not found";
    private String SPLIT_LENGTH_CHARACTER = "-";

    @When("^Open weather site$")
    public void open_weather_site() {
        windowAndFrameBaseSteps.open_the(SITE_URL);
    }

    @When("^Input ['\"]([^'\"]*)['\"], search for city ['\"]([^'\"]*)['\"]$")
    public void search_for_city(String explaination, String cityToSearch) {
        String searchText;
        int length = 0;
        String searchDataType = "";
        if(cityToSearch.contains(SPLIT_LENGTH_CHARACTER)) {
            length = Integer.parseInt(cityToSearch.substring(cityToSearch.indexOf(SPLIT_LENGTH_CHARACTER) + 1));
            searchDataType = cityToSearch.substring(0, cityToSearch.indexOf(SPLIT_LENGTH_CHARACTER));
        }

        switch (searchDataType){
            case Constant.RANDOM_STRING_ALPHABET_VALUE:
                searchText = RandomStringUtils.random(length, Constant.CHARACTERS);
                break;
            default:
                searchText = cityToSearch;
        }
//        TestLogger.info("Searching text = " + searchText);
        coreActionFormBaseSteps.wait_for_the_element_to_be_clickable(SEARCH_TOP_TEXTBOX);
        coreActionFormBaseSteps.press_tab_key(2);
        coreActionFormBaseSteps.enter_into_the_field_with_value_by_java_script(SEARCH_TOP_TEXTBOX, searchText);
        coreActionFormBaseSteps.press_enter_key();
    }


    @When("^Click on link in result list$")
    public void click_link_in_result_list() {
        coreActionFormBaseSteps.wait_for_the_element_to_be_clickable(SEARCH_RESULT_LINK);
        coreActionFormBaseSteps.click_on_the_element_by_javascript(SEARCH_RESULT_LINK);
    }

    @Then("^It should display no result found$")
    public void display_not_found() {
        verificationBaseSteps.the_text_element_should_contain(SEARCH_RESULT_NOTFOUND_LABEL, SEARCH_RESULT_NOTFOUND_DISPLAY_TEXT);
    }
}
