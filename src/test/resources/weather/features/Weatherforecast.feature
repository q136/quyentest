@search
Feature: Check search feature on weather forecast

 @smoke
  Scenario Outline: Smoke for Search for city and check its current weather
    When Open weather site
    And Input '<explaination>', search for city '<city>'
    And Click on link in result list
    Then Search detail should display current weather for location '<locationResult>'
    Examples:
      | explaination                                 | city      | locationResult       |
      | existed city without space                   | Hanoi     | Hanoi, VN            |


  @happycase
  Scenario Outline: Search for city and check its current weather
    When Open weather site
    And Input '<explaination>', search for city '<city>'
    And Click on link in result list
    Then Search detail should display current weather for location '<locationResult>'
    Examples:
      | explaination                                 | city      | locationResult       |
      | existed city without space                   | Hanoi     | Hanoi, VN            |
      | existed city with space                      | Ha noi    | Hanoi, VN            |
      | existed city displayed in Vietnamese         | Hà Nội    | Hanoi, VN            |
      | city name in search and result are different | Sài Gòn   | Ho Chi Minh City, VN |
      | name of country                              | Vietnam   | Vietnam, VN          |
      | name in uppercase                            | HẢI PHÒNG | Haiphong, VN         |
      | name of district                             | Phú Thọ   | Tỉnh Phú Thọ, VN     |

  @unhappycase
  Scenario Outline: Search for invalid city and it should display Not Found result
    When Open weather site
    And Input '<explaination>', search for city '<city>'
    Then It should display no result found
    Examples:
      | explaination                     | city         |
      | not existed city                 | abcd         |
      | city with incorrect country code | Hanoi, UK    |
      | incorrect city name              | hhanoi       |
      | random data in length 255        | random-255   |
      | random data in length 10000      | random-10000 |





