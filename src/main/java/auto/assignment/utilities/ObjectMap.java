package auto.assignment.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ObjectMap {

    static Properties properties;


    /**
     * Get locator string from element properties
     *
     * @param key
     * @return ret
     * @throws Exception
     */
    public static String getActualValueFromObjectRepository(String key) {
        // Read value using the logical name as Key
        if (Configuration.getPropertyValue(key) != null) {
            return Configuration.getPropertyValue(key);
        } else {
            String value;
            try {
                value = properties.getProperty(key);
            } catch (Exception e) {
                value = null;
            }
            if (value != null && !value.isEmpty()) {
                return value;
            } else
                return key;
        }
    }


    /**
     * loading locator element from folder
     *
     * @param folderName
     * @return ret
     * @throws IOException
     */
    public static Properties loadObjectPropertiesFromFolder(String folderName) {
        File directory;
        FileInputStream fs = null;
        directory = new File(folderName);
        if (directory.isDirectory()) {
            properties = new Properties();
            try {
                for (File file : directory.listFiles()) {
                    fs = new FileInputStream(file.getAbsoluteFile());
                    properties.load(fs);
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return properties;
    }
}