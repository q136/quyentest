package auto.assignment.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Properties;

public class Helper {
    public static Properties loadConfigSys() throws IOException {
        String fs = File.separator;
        FileInputStream is = new FileInputStream(
                (System.getProperty("user.dir") + "/serenity.properties").replace("/", fs).replace("\\", fs));
        Properties pros = new Properties();
        pros.load(is);
        return pros;
    }

    public static String formatDateTimeInMonthDateTime(long currentTimeInMilliseconds){
        SimpleDateFormat dateFormat = new SimpleDateFormat(" MMM d, hh:mma");
        DateFormatSymbols symbols = new DateFormatSymbols(Locale.getDefault());
        symbols.setAmPmStrings(new String[] { "am", "pm" });
        dateFormat.setDateFormatSymbols(symbols);

        return dateFormat.format(currentTimeInMilliseconds);
    }

}
