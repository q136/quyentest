package auto.assignment.utilities;
public class Constant {
    public static final String RANDOM_STRING_ALPHABET_VALUE = "random";
    public static final String CHARACTERS = "ABCDEFGHIJKHLMNOPTUXYZ1234567890abcdfghijklmnopstuvwxyz";
    public static final String CONFIGURATION_KEY_OBJECT_PROPERTIES = "object.properties";
  }
