package auto.assignment.utilities;

import auto.assignment.ui.coreactions.PageCore;
import com.google.inject.Inject;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigValue;
import net.thucydides.core.ThucydidesSystemProperty;
import net.thucydides.core.configuration.SystemPropertiesConfiguration;
import net.thucydides.core.requirements.SearchForFilesWithName;
import net.thucydides.core.util.EnvironmentVariables;
import net.thucydides.core.util.LocalPreferences;
import net.thucydides.core.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.*;

import static org.apache.commons.lang3.StringUtils.strip;

public class PropertiesFileLocalPreferences implements LocalPreferences {

	public static final String TYPESAFE_CONFIG_FILE = "serenity.conf";
	private File workingDirectory;
	private File homeDirectory;
	private File mavenModuleDirectory;
	private final EnvironmentVariables environmentVariables;
	private static final Logger LOGGER = LoggerFactory.getLogger(PropertiesFileLocalPreferences.class);


	@Inject
	public PropertiesFileLocalPreferences(EnvironmentVariables environmentVariables) {
		this.environmentVariables = environmentVariables;
		this.homeDirectory = new File(System.getProperty("user.home"));
		this.workingDirectory = new File(System.getProperty("user.dir"));
		final String mavenBuildDir = System.getProperty(SystemPropertiesConfiguration.PROJECT_BUILD_DIRECTORY);
		if (!StringUtils.isEmpty(mavenBuildDir)) {
			this.mavenModuleDirectory = new File(mavenBuildDir);
		} else {
			this.mavenModuleDirectory = this.workingDirectory;
		}
	}

	public void loadPreferences() throws IOException {

		updatePreferencesFrom(preferencesIn(preferencesFileWithAbsolutePath()),
				preferencesIn(legacyPreferencesFileWithAbsolutePath()),
				typesafeConfigPreferencesInCustomDefinedConfigFile(), typesafeConfigPreferences(),
				preferencesIn(preferencesFileInMavenModuleDirectory()),
				preferencesIn(preferencesFileInMavenParentModuleDirectory()),
				preferencesIn(preferencesFileInWorkingDirectory()),
				preferencesIn(legacyPreferencesFileInWorkingDirectory()),
				preferencesIn(preferencesFileInHomeDirectory()), preferencesIn(legacyPreferencesFileInHomeDirectory()),
				preferencesInClasspath());
	}

	private Properties preferencesInClasspath() throws IOException {
		try (InputStream propertiesOnClasspath = propertiesInputStream()) {
			if (propertiesOnClasspath != null) {
				Properties localPreferences = new Properties();
				localPreferences.load(propertiesOnClasspath);
				return localPreferences;
			}
		}
		return new Properties();
	}

	private InputStream propertiesInputStream() {
		InputStream input = Thread.currentThread().getContextClassLoader()
				.getResourceAsStream(defaultPropertiesFileName());
		if (input == null) {
			input = Thread.currentThread().getContextClassLoader().getResourceAsStream(legacyPropertiesFileName());
		}
		return input;
	}

	private Properties typesafeConfigPreferencesInCustomDefinedConfigFile() {

		Optional<File> providedConfigFile = defaultPropertiesConfFile();
		if (!providedConfigFile.isPresent()) {
			return new Properties();
		}

		Set<Map.Entry<String, ConfigValue>> preferences = typesafeConfigFile(providedConfigFile.get()).entrySet();
		return getPropertiesFromConfig(preferences);
	}

	private Properties typesafeConfigPreferences() {
		String configProjectFile = PageCore.getConfig("project.env.config");
		if (configProjectFile.contentEquals("serenity.conf")) {
			return defaultPropertiesConfFile().filter(File::exists)
					.map(configFile -> getPropertiesFromConfig(typesafeConfigFile(configFile).entrySet()))
					.orElse(getPropertiesFromConfig(ConfigFactory.load(TYPESAFE_CONFIG_FILE).entrySet()));
		} else {
			return defaultPropertiesConfFile().filter(File::exists)
					.map(configFile -> getPropertiesFromConfig(typesafeConfigFile(configFile).entrySet()))
					.orElse(getPropertiesFromConfig(ConfigFactory.load("configProjectFile").entrySet()));
		}
	}

	private Config typesafeConfigFile(File configFile) {

		Config commandLineSpecifiedProperties = ConfigFactory
				.parseMap(environmentVariables.simpleSystemPropertiesAsMap());

		// TODO: Cache resolved config for the aggregate phase
		return ConfigFactory.parseFile(configFile).resolveWith(commandLineSpecifiedProperties);

	}

	private Properties getPropertiesFromConfig(Set<Map.Entry<String, ConfigValue>> preferences) {
		Properties properties = new Properties();
		for (Map.Entry<String, ConfigValue> preference : preferences) {
			properties.put(preference.getKey(), strip(preference.getValue().render(), "\""));
		}
		return properties;
	}

	private void updatePreferencesFrom(Properties... propertySets) throws IOException {
		for (Properties localPreferences : propertySets) {
			PropertiesUtil.expandPropertyAndEnvironmentReferences(System.getenv(), localPreferences);
			setUndefinedSystemPropertiesFrom(localPreferences);
		}
	}

	private Properties preferencesIn(File preferencesFile) throws IOException {
		Properties preferenceProperties = new Properties();
		if (preferencesFile.exists()) {
			try (InputStream preferences = new FileInputStream(preferencesFile)) {
				LOGGER.debug("LOADING LOCAL PROPERTIES FROM {} ", preferencesFile.getAbsolutePath());
				preferenceProperties.load(preferences);
			}
		}
		return preferenceProperties;
	}

	private void setUndefinedSystemPropertiesFrom(Properties localPreferences) {
		Enumeration<?> propertyNames = localPreferences.propertyNames();
		while (propertyNames.hasMoreElements()) {
			String propertyName = (String) propertyNames.nextElement();
			String localPropertyValue = localPreferences.getProperty(propertyName);
			String currentPropertyValue = environmentVariables.getProperty(propertyName);

			if ((currentPropertyValue == null) && (localPropertyValue != null)) {
				LOGGER.debug(propertyName + "=" + localPropertyValue);
				environmentVariables.setProperty(propertyName, localPropertyValue);
			}
		}
	}

	private File preferencesFileInHomeDirectory() {
		return new File(homeDirectory, defaultPropertiesFileName());
	}

	private File legacyPreferencesFileInHomeDirectory() {
		return new File(homeDirectory, legacyPropertiesFileName());
	}

	private File preferencesFileInWorkingDirectory() {
		return new File(workingDirectory, defaultPropertiesFileName());
	}

	private File preferencesFileInMavenModuleDirectory() {
		return new File(mavenModuleDirectory, defaultPropertiesFileName());
	}

	private File preferencesFileInMavenParentModuleDirectory() {
		File parentModuleDirectory = mavenModuleDirectory.getParentFile();
		return new File(parentModuleDirectory, defaultPropertiesFileName());
	}

	private File legacyPreferencesFileInWorkingDirectory() {
		return new File(workingDirectory, legacyPropertiesFileName());
	}

	private File preferencesFileWithAbsolutePath() {
		return new File(defaultPropertiesFileName());
	}

	private File legacyPreferencesFileWithAbsolutePath() {
		return new File(legacyPropertiesFileName());
	}

	private final String PROPERTIES = ThucydidesSystemProperty.PROPERTIES.getPropertyName();

	private Optional<File> defaultPropertiesConfFile() {

		List<String> possibleConfigFileNames = new ArrayList<>();

		environmentVariables.optionalProperty(System.getProperty(PROPERTIES)).ifPresent(possibleConfigFileNames::add);
		serenityConfFileInASensibleLocation().ifPresent(possibleConfigFileNames::add);

		return possibleConfigFileNames.stream().map(File::new).filter(File::exists).findFirst();
	}

	private final String SERENITY_CONF_FILE = "(.*)[\\/\\\\]?src[\\/\\\\]test[\\/\\\\]resources[\\/\\\\]serenity.conf";

	private Optional<String> serenityConfFileInASensibleLocation() {
		try {
			String configProjectFile = "(.*)[\\/\\\\]?src[\\/\\\\]test[\\/\\\\]resources[\\/\\\\]"
					+ PageCore.getConfig("project.env.config");
			if (configProjectFile.contentEquals("serenity.conf")) {
				return SearchForFilesWithName.matching(Paths.get("."), SERENITY_CONF_FILE).getMatchingFiles().stream()
						.findFirst().map(path -> path.toAbsolutePath().toString());
			} else {
				return SearchForFilesWithName.matching(Paths.get("."), configProjectFile).getMatchingFiles().stream()
						.findFirst().map(path -> path.toAbsolutePath().toString());
			}
		} catch (IOException e) {
			return Optional.empty();
		}
	}

	private String defaultPropertiesFileName() {

		return optionalEnvironmentVariable(System.getProperty(PROPERTIES))
				.orElse(optionalEnvironmentVariable(System.getenv(PROPERTIES)).orElse("serenity.properties"));
	}

	private String legacyPropertiesFileName() {
		return optionalEnvironmentVariable(System.getProperty(PROPERTIES))
				.orElse(optionalEnvironmentVariable(System.getenv(PROPERTIES)).orElse("thucydides.properties"));
	}

	private Optional<String> optionalEnvironmentVariable(String value) {
		return Optional.ofNullable(value);
	}

}
