package auto.assignment.utilities;

import net.serenitybdd.core.environment.EnvironmentSpecificConfiguration;
import net.thucydides.core.util.EnvironmentVariables;

import java.io.File;

public class Configuration {
    private static EnvironmentVariables environmentVariables = SystemEnvironmentVariables.createEnvironmentVariables();

    public Configuration() {
        super();
    }

      public static String getPropertyValue(String key) {
        try {
            return EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(key);
        } catch (Exception ext) {
            return null;
        }
    }


    public static String getObjectFolder() {
    	String value = null; 
        try {
			value = System.getProperty("user.dir") + File.separator
			        + EnvironmentSpecificConfiguration.from(environmentVariables).getProperty(Constant.CONFIGURATION_KEY_OBJECT_PROPERTIES)
			        .replace("\\", File.separator).replace("/", File.separator);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        return value;
    }
}
