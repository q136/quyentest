package auto.assignment.ui.steps;

import auto.assignment.ui.coreactions.CoreActionForm;
import auto.assignment.ui.coreactions.PageCore;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.annotations.Steps;
import org.openqa.selenium.WebElement;

public class CoreActionFormBaseSteps extends EndUserBaseSteps {

    /*
     */
    private static final long serialVersionUID = 1L;
    PageCore pageCore;

    CoreActionForm coreActionForm;

    @Step
    public void enter_into_the_field_with_value(String element, String value, String... srcvar) {
        // First, perform click
        WebElement webElement = pageCore.getWebElement(element, srcvar);
        webElement.click();

        // Then send the value
        webElement = pageCore.getWebElement(element, srcvar);
        webElement.clear();

        webElement.sendKeys(value);
    }


    @Step
    public void enter_into_the_field_with_value_by_java_script(String element, String value, String... srcvar) {
        coreActionForm.setFieldValueByJavascript(element, value, srcvar);
    }

    @Step
    public void press_tab_key(Integer number) {
        coreActionForm.pressTabKey(number);
    }

    @Step
    public void press_enter_key() {
        coreActionForm.pressEnterKey();
    }

    @Step
    public void click_on_the_element_by_javascript(String element, String... srcvar) {
        coreActionForm.clickByJavascript(element, srcvar);
    }

    @Step
    public void click_on_the_element(String element, String... srcvar) {
		coreActionForm.clickOnElement(element, srcvar);
    }

}
