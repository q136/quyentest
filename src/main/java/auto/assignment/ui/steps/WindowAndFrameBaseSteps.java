package auto.assignment.ui.steps;

import auto.assignment.ui.coreactions.PageCore;
import auto.assignment.ui.coreactions.WindowsAndFrames;
import net.thucydides.core.annotations.Step;

public class WindowAndFrameBaseSteps extends EndUserBaseSteps {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    PageCore pageCore;
    WindowsAndFrames windowsAndFrames;


    @Step
    public void open_the(String url) {
        String urls = pageCore.getActualValueFromObjectRepository(url);
        try {
            pageCore.open();
            windowsAndFrames.navigateToUrl(pageCore.getActualValueFromObjectRepository(urls));
        } catch (Exception e) {
            pageCore.open();
            windowsAndFrames.navigateToUrl(pageCore.getActualValueFromObjectRepository(urls));
        }
    }
}
