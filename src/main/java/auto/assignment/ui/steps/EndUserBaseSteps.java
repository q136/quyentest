package auto.assignment.ui.steps;

import auto.assignment.ui.coreactions.PageCore;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.steps.ScenarioSteps;

public class EndUserBaseSteps extends ScenarioSteps {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    PageCore pageCore;

    @Step
    public void close_browser() {
        pageCore.closeBrowser();
		pageCore.clearSession();
    }

    @Step
    public void clear_session() {
        getDriver().manage().deleteAllCookies();
    }



    @Step
    public void wait_for_the_element_to_be_clickable(String element, String... srcvar) {
        pageCore.waitUntilElementTobeClickable(element, srcvar);
    }


    @Step
    public void wait_for_the_element_to_be_visibile(String element, String... srcvar) {
        pageCore.waitUntilElementVisibility(element, srcvar);
    }

    @Step
    public void wait_for_any_text_to_be_appeared(String textValue) {
        pageCore.waitForTextToAppear(textValue);
    }


}
