package auto.assignment.ui.steps;

import auto.assignment.ui.coreactions.PageCore;
import auto.assignment.utilities.TestLogger;
import net.thucydides.core.annotations.Step;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class VerificationBaseSteps extends EndUserBaseSteps {
    /*
     */
    private static final long serialVersionUID = 1L;
    PageCore pageCore;

    @Step
    public void the_text_element_should_be(String element, String value, String... srcvar) {
        assertThat(pageCore.getCurrentTextOfElement(element, srcvar),
                equalTo(pageCore.getActualValueFromObjectRepository(value).trim()));
    }

    @Step
    public void the_text_element_should_not_be(String element, String value, String... srcvar) {
        assertThat(pageCore.getCurrentTextOfElement(element, srcvar),
                not(equalTo(pageCore.getActualValueFromObjectRepository(value).trim())));
    }

    @Step
    public void the_text_element_should_contain(String element, String value, String... srcvar) {
        try {
            wait_for_any_text_to_be_appeared(pageCore.getActualValueFromObjectRepository(value));
            assertThat(pageCore.getCurrentTextOfElement(element, srcvar),
                    containsString(pageCore.getActualValueFromObjectRepository(value).trim()));
        } catch (NullPointerException nullPointerException) {
            TestLogger.info("Element is not present!");
        }
    }

    @Step
    public void the_text_element_should_not_contain(String element, String value, String... srcvar) {
        // wait_for_any_text_to_be_disappread(pageCore.getActualValueFromObjectRepository(value));
        assertThat(pageCore.getCurrentTextOfElement(element, srcvar),
                not(containsString(pageCore.getActualValueFromObjectRepository(value).trim())));
    }


}
