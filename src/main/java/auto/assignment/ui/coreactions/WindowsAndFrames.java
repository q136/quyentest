package auto.assignment.ui.coreactions;

import auto.assignment.utilities.TestLogger;

public class WindowsAndFrames extends PageCore {
	/**
	 * navigate to url
	 * 
	 * @param url
	 */
	public void navigateToUrl(String url) {
		maximizeBrowser();
		TestLogger.info(url);
		getDriver().navigate().to(url);
	}

}
