package auto.assignment.ui.coreactions;

import auto.assignment.utilities.TestLogger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

public class CoreActionForm extends PageCore {

    /********** click action ***********************/
    /**
     * click by javascript
     */
    public void clickByJavascript(String element, String... srcvar) {
        try {
            WebElement onElement = getWebElement(element, srcvar);
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", onElement);
        } catch (ElementNotInteractableException elementNotInteractableException) {
            WebElement onElement = getWebElement(element, srcvar);
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", onElement);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Element not present!");
        } catch (NoSuchWindowException noSuchWindowException) {
            System.out.println("Window is already closed");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * press enter key
     */
    public void pressEnterKey() {
        Actions action = new Actions(getDriver());
        action.sendKeys(Keys.RETURN);
        action.perform();
        action.release();
    }

    /**
     * press tab key
     */
    public void pressTabKey(Integer number) {
        for (int i = 0; i < number; i++) {
            Actions action = new Actions(getDriver());
            action.sendKeys(Keys.TAB).perform();
            action.release();
        }
    }
    public void setFieldValueByJavascript(String element, String value, String... srcvar) {
        WebElement onElement = getWebElement(element, srcvar);
        onElement.clear();
        if (!onElement.getTagName().equals("input")) {
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].innerHTML = '" + value + "'", onElement);
        } else {
            ((JavascriptExecutor) getDriver()).executeScript("arguments[0].value = '" + value + "'", onElement);
        }
    }

    /**
     * click on Element
     *
     * @param element
     */
    public void clickOnElement(String element, String... srcvar) {
        WebElement onElement = getWebElement(element, srcvar);
        for (int tick = 0; tick < DEFAULT_TIMEOUT / WAIT_INTERVAL; tick++) {
            if (onElement.isEnabled() == true) {
                break;
            }
            pause(WAIT_INTERVAL / 1000);
        }

            try {
                TestLogger.info("click normally");
                onElement.click();
            } catch (ElementNotInteractableException | StaleElementReferenceException elementNotInteractableException) {
                TestLogger.info("click normally");
                onElement.click();
            } catch (NullPointerException nullPointerException) {
                System.out.println("Element not present!");
            } catch (NoSuchWindowException noSuchWindowException) {
                System.out.println("Window is already closed");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }



}
