package auto.assignment.ui.coreactions;

import auto.assignment.utilities.Configuration;
import auto.assignment.utilities.Helper;
import auto.assignment.utilities.ObjectMap;
import auto.assignment.utilities.TestLogger;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

@DefaultUrl("http://google.com/")
public class PageCore extends PageObject {

    public static Properties properties;
    public static String defaultWindow = "";
    public static int DEFAULT_TIMEOUT;
    public static int WAIT_INTERVAL;


    public PageCore() {
//        String geckoDriver = getConfig("webdriver.gecko.driver");
        // String firefoxBinPath = getConfig("webdriver.firefox.bin");
        DEFAULT_TIMEOUT = Integer.valueOf(getConfig("timeout"));
        WAIT_INTERVAL = 10000;
//        String marionetteEnable = getConfig("webdriver.firefox.marionette");
//        System.setProperty("webdriver.gecko.driver", geckoDriver);
//        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, marionetteEnable);
        // System.setProperty("webdriver.firefox.bin", firefoxBinPath);
        String objects = Configuration.getObjectFolder();
        properties = ObjectMap.loadObjectPropertiesFromFolder(objects);
    }

    /**
     * Gets system config else config.properties
     *
     * @param key config key
     * @return config value
     */
    static public String getConfig(String key) {
        String retConfig = null;
        Properties config;
        try {
            config = Helper.loadConfigSys();
            retConfig = (System.getProperty(key) != null ? System.getProperty(key) : config.getProperty(key));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return retConfig;
    }

    /**
     * maximize page
     */
    public void maximizeBrowser() {
        getDriver().manage().window().setSize(new Dimension(1460, 1024));
//        getDriver().manage().window().maximize();
    }


    /**
     * close browser
     *
     * @throws IOException
     */
    public void closeBrowser() {
        if (!getConfig("webdriver.driver").contains("appium")) {
            getDriver().close();
            getDriver().quit();
        }
    }

    /**
     * wait for milisecond
     *
     * @param second
     */
    public void pause(int second) {
        try {
            Thread.sleep(second * 1000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * get location of element
     *
     * @param target
     * @return ObjectMap.getActualValueFromObjectRepository(target)
     */
    public String getActualValueFromObjectRepository(String target) {
        return ObjectMap.getActualValueFromObjectRepository(target).replace("\t", "");
    }


    /**
     * get element
     *
     * @param target
     * @param srcvar
     * @return WebElement
     */
    public WebElement getWebElement(String target, String... srcvar) {
        String locator = getActualValueFromObjectRepository(target);
        for (int i = 0; i < srcvar.length; i++) {
            TestLogger.info("param " + i);
            locator = locator.replace("${param" + (i + 1) + "}",
                    getActualValueFromObjectRepository(srcvar[i]).trim());
            TestLogger.info(locator);
        }
        TestLogger.info(target + ": " + locator);
        try {
            TestLogger.info("begin finding element");
            WebDriverWait wait = new WebDriverWait(getDriver(), DEFAULT_TIMEOUT / 1000);
            TestLogger.info("start waiting to find element");
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(getObject(locator)));
            TestLogger.info("finish waiting to find element");
            return getDriver().findElement(getObject(locator));
        } catch (StaleElementReferenceException staleElementReferenceException) {
            TestLogger.info("go to catch exception - begin finding element");
            WebDriverWait wait = new WebDriverWait(getDriver(), DEFAULT_TIMEOUT / 1000);
            TestLogger.info("go to catch exception - start waiting to find element");
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(getObject(locator)));
            TestLogger.info("go to catch exception - finish waiting to find element");
            return getDriver().findElement(getObject(locator));
        }
        catch (Exception e) {
            e.getMessage();
        }
        TestLogger.error("Can not find element after " + DEFAULT_TIMEOUT / 1000 + " seconds " + getObject(locator));
        return null;
    }

    /**
     * get list elements
     *
     * @param target
     * @param srcvar
     * @return List<WebElement>
     */
    public List<WebElement> getWebElements(String target, String... srcvar) {
        String locator = getActualValueFromObjectRepository(target);
        TestLogger.info(locator);
        for (int i = 0; i < srcvar.length; i++) {
            TestLogger.info("param " + i);
            locator = locator.replace("${param" + (i + 1) + "}",
                    getActualValueFromObjectRepository(srcvar[i]));
            TestLogger.info(locator);
        }
        WebElement elem = getWebElement(target, srcvar);
        if (elem != null) {
            return getDriver().findElements(getObject(locator));
        } else
            return null;
    }

    /**
     * get locator
     *
     * @param locator
     * @return ret
     */
    public By getObject(String locator) {
        By by = null;
        try {
            if (locator.startsWith("id=")) {

                locator = locator.substring(3);
                by = By.id(locator);

            } else if (locator.startsWith("name=")) {

                locator = locator.substring(5);
                by = By.name(locator);

            } else if (locator.startsWith("css=")) {

                locator = locator.substring(4);
                by = By.cssSelector(locator);

            } else if (locator.startsWith("linkText=")) {

                locator = locator.substring(9);
                by = By.linkText(locator);

            } else if (locator.startsWith("xpath=")) {
                locator = locator.substring(6);
                by = By.xpath(locator);
            } else if (locator.startsWith("tagname=")) {
                locator = locator.substring(8);
                by = By.tagName(locator);
            } else if (locator.startsWith("classname=")) {
                locator = locator.substring(9);
                by = By.className(locator);
            } else {
                by = By.xpath(locator);
            }

            return by;
        } catch (Exception e) {
            e.getMessage();
        }

        return null;
    }

    /**
     * get text of element
     *
     * @param element
     * @return ret
     */
    public String getCurrentTextOfElement(String element, String... srcvar) {
        return getWebElement(element, srcvar).getText().trim();
    }

    /**
     * wait until element to be clickable
     *
     * @param element
     */
    public void waitUntilElementTobeClickable(String element, String... srcvar) {
        try {
            new WebDriverWait(getDriver(), 60).ignoring(NoAlertPresentException.class)
                    .until(ExpectedConditions.elementToBeClickable(getWebElement(element, srcvar)));
        } catch (StaleElementReferenceException staleElementReferenceException) {
            new WebDriverWait(getDriver(), 60).ignoring(NoAlertPresentException.class)
                    .until(ExpectedConditions.elementToBeClickable(getWebElement(element, srcvar)));
        } catch (NullPointerException nullPointerException) {
            TestLogger.info("Element is not present!");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * wait until element visible
     *
     * @param element
     */
    public void waitUntilElementVisibility(String element, String... srcvar) {
        try {
            new WebDriverWait(getDriver(), 60).ignoring(NoAlertPresentException.class)
                    .until(ExpectedConditions.visibilityOf(getWebElement(element, srcvar)));
        } catch (Exception e) {
            new WebDriverWait(getDriver(), 60).ignoring(NoAlertPresentException.class)
                    .until(ExpectedConditions.visibilityOf(getWebElement(element, srcvar)));
        }
    }

    /**
     * clear session
     */
    public void clearSession() {
        getDriver().manage().deleteAllCookies();
    }
}