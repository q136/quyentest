# Test Automation Framework
###
  * [How to setup and run test](#how-to-setup-and-run-test)
  
## Setting up the development environment
### Install required applications 

Git - For version control - [More instructions on installing](http://git-scm.com/book/en/Getting-Started-Installing-Git)

    Linux:
      yum install git-core
      apt-get install git

    Mac:
      ( http://code.google.com/p/git-osx-installer )
      sudo port install git-core +svn +doc +bash_completion +gitweb

    Windows:
      ( http://msysgit.github.com/ )

Java - [https://java.com/en/download/help/download_options.xml](https://java.com/en/download/help/download_options.xml)

Maven - For building project and running script from command line

    Linux - Mac - Windows
      https://www.baeldung.com/install-maven-on-windows-linux-mac

Editor tools (Intellij)

    Linux - Mac - Windows
    	Download from https://www.jetbrains.com/idea/download

## Structure of the project

This project consists mainly of three parts. The main, the test and other configuration file

### main

#### main-java-code
##### ui

    1. ui.coreactions    # define common actions for frontend testing 
    2. ui.steps          # define common steps for frontend testing. This steps use methods that defines from ui.coreactions.

##### utilities

    1. Define class/object that use in other class

#### main-resources

    1. Contains driver for chrome/firefox
    2. Contains log4j file and environment file (serenity.conf)
 
### test

#### test-java-code
##### project-package

    1. Each project will define its package.
    2. Each project will define specific steps in this package

##### scenario-steps

    1. Define common steps that use in all projects 
##### runner

    1. Use to run test, map feature file with backend actions

#### test-resources

    1. Contains feature file. Each project will create its folder and put its feature file to this folder
    2. Contains object file. Each project will create its folder and put its properties file to this folder
    3. Contains configuration file of each project. Each project will have file: project_name.conf. 
        This file will contains environment information (dev, staging,...) of project.
   
## how-to-run test
    1. Clone project:          
        git clone https://gitlab.com/q136/quyentest.git
    2. Create file projectname.conf and add to src/test/resources. 
       This file defines environment information of dev, staging env. You can check file serenity.conf as example
    3. Run test: there are 2 ways to run test
    - Run test from editor tool
    - Run test from terminal: mvn clean verify -Dproject.env.config=${project_config_file} -Denvironment=${env} -Dcucumber.filter.tags="@tag"
        mvn clean verify -Denvironment=dev -Dproject.env.config=serenity.conf
    if there is no param, it will use default values in configuration file

    - To run with chrome browser, use this example:
        mvn clean verify -Dcontext=chrome -Dwebdriver.driver=chrome -Dcucumber.filter.tags="@search"
    - To run with firefox browser, use this example:
        mvn clean verify -Dcontext=firefox -Dwebdriver.driver=firefox -Dcucumber.filter.tags="@search"
    4. View report:
        Report is stored in project directory, under this path: target/site/serenity/index.html.Open it in browser to see the result
## How to run test in CI/CD
    I use Jenkins for this purpose. 
    The pipeline job is configured in Jenkinsfile. Test are run parallel like this:
        https://drive.google.com/file/d/1vPRM9EVDVw5oh2C_ewBrBZxb8hPjVIOB/view?usp=sharing
    - To run Jenkins pipeline, Go to Jenkins, create a pipeline job and set it like this:
        https://drive.google.com/file/d/1ChoKZ55-XtJoAiOJvcYRBXhZKVNGOY6-/view?usp=sharing
    - When the test completed, you can view the report:
        https://drive.google.com/file/d/16REUVfOxWbh-55pE7AxHpylLfb2DMACe/view?usp=sharing
        The report will look like this:
        https://drive.google.com/file/d/17FqG8NbntvfFGHPmUnyfhX6UUXHFWk-g/view?usp=sharing
        https://drive.google.com/file/d/14tTV2fzOcILAV-t7NnGIBbM8JRntD6uo/view?usp=sharing
        
 
